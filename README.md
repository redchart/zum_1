Znázornění algoritmů: DFS(iter), BFS(iter), Random Search, Greedy Search, A*

Návod použití

1. Naclonovat nebo stáhnout .zip repositáře
2. Otevřít demo.html v prohlížeči.
3. Zvolit algoritmus, rychlost běhu a vstupní soubor ze složky dataset.tar.gz.
4. Zmáčknout zelené tlačítko "Start".
5. Pokud chcete pustit program pro jiné vstupní parametry stačí buď restartovat stránku  nebo zmáčknout tlačítko "Reset page" 


==============================================================================


Visualization of these alghoritms: DFS(iter), BFS(iter), Random Search, Greedy Search, A*

How to use

1. Clone or download a .zip repository
2. Open demo.html in a browser.
3. Select the algorithm, select speed and input the file from dataset.tar.gz folder.
4. Press the green "Start" button.
5. If you want to run the program for other input parameters, just restart the page or press the "Reset page" button