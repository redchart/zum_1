let nodes_expanded_el = document.getElementById('nodes_expanded_el');
let path_length_el = document.getElementById('path_length_el');
let expanded_nodes = 0;
let path_length = 0;
let timeout_el = document.getElementById("timeout");
let timeout = 0;
let algo_name = 'dfs_iter';
let file_data;
let algo_name_el = document.getElementById('algo_name');

let algos = {
    'dfs_iter' : dfs_iter,
    'bfs_iter' : bfs_iter,
    'random_search' : random_search,
    'greedy_search' : greedy_search,
    'a_star' : a_star
}

algo_name_el.addEventListener("change", function(){
    algo_name = this.value;
});

timeout_el.addEventListener("change", function(){
    timeout = this.value;
});
function readSingleFile(e) {
    var file = e.target.files[0];
    if (!file) {
      return;
    }
    var reader = new FileReader();
    reader.onload = function(e) {
        file_data = e.target.result;
    };
    reader.readAsText(file);
  }
  
  function run_algo() {
    var lines = file_data.split('\n');

    var rowsAmount = lines.length - 3;
    var colsAmount = lines[0].length;

    var matrix = createMatrix(lines, rowsAmount, colsAmount);
    printMatrix(matrix, rowsAmount, colsAmount);
    var nodes = createTable(matrix, rowsAmount, colsAmount);

    generateNeighbors(nodes, rowsAmount, colsAmount);

    var first_row_lines = lines[rowsAmount].split(',');
    var second_row_lines = lines[rowsAmount + 1].split(',');
    var game_start_i =  first_row_lines[0].replace( /^\D+/g, '');
    var game_start_j = first_row_lines[1].replace( /^\D+/g, '');
    var game_end_i = second_row_lines[0].replace( /^\D+/g, '');
    var game_end_j = second_row_lines[1].replace( /^\D+/g, '');

    if(game_start_i > rowsAmount || game_start_j > colsAmount){
        var start = nodes[game_start_j][game_start_i];
    }else{
        var start = nodes[game_start_i][game_start_j];
    }

    if(game_end_i > rowsAmount || game_end_j > colsAmount){
        var target = nodes[game_end_j][game_end_i];
    }else{
        var target = nodes[game_end_i][game_end_j];
    }
    target.dom.classList.add("td-end");

    algos[algo_name](start,target)
    //bfs_iter();
    //element.textContent = contents;
  }

  
  
document.getElementById('file-input').addEventListener('change', readSingleFile, false);

class Node{
    constructor(row, col, dom) {
        this.row = row;
        this.col = col;
        this.dom = dom;
        this.neighbors = [];
        this.visited = false;
        this.wave = 1;
        this.parent = null;
        this.closed = false;
        this.distance = null;
        this.star_distance = Infinity;
      }
}

function generateNeighbors(nodes,rows, cols){
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++){
            var node = nodes[i][j];
            if(node == null){
                continue;
            }
            var up = (node.row + 1 < rows && node.col < cols) ? nodes[node.row + 1][node.col]  : undefined;
            var right = (node.row < rows && node.col + 1 < cols) ? nodes[node.row][node.col + 1] : undefined;
            var down = (node.row - 1 < rows && node.col < cols) ? nodes[node.row - 1][node.col] : undefined;
            var left = (node.row < rows && node.col - 1 < cols) ? nodes[node.row][node.col - 1] : undefined;

            if(up !== undefined && up !== null){
                node.neighbors.push(up)
            }
            if(right !== undefined && right !== null){
                node.neighbors.push(right)
            }
            if(down !== undefined  && down !== null){
                node.neighbors.push(down)
            }
            if(left !== undefined  && left !== null){
                node.neighbors.push(left)
            }
        }
    }
}

function createTable(matrix, rowsAmount, colsAmount){
    var table = document.getElementById('gameMatrix');
    var nodes = new Array(rowsAmount)
    for (var i = 0; i < rowsAmount; i++) {
        var row = document.createElement("tr");
        row.classList = "table-tr";
        nodes[i] = new Array(colsAmount);

        for (var j = 0; j < colsAmount; j++) {
            var col = document.createElement("td");
            if(matrix[i][j] == "X"){
                col.classList = "table-td td-blocked";
                nodes[i][j] = null;
            }else{
                
                col.classList = "table-td";
                var node = new Node(i,j,col);
                nodes[i][j] = node;
            }
            row.appendChild(col);
        }
        table.appendChild(row);
    }
    return nodes;
}


function createMatrix(lines,rowsAmount, colsAmount){
    var matrix = new Array(rowsAmount);

    for (var i = 0; i < rowsAmount; i++) {
        matrix[i] = new Array(colsAmount);
        for (var j = 0; j < colsAmount; j++) {
            matrix[i][j] = lines[i][j];
        }
    }

    return matrix;
}

function printMatrix(matrix, rowsAmount, colsAmount){
    for (var i = 0; i < rowsAmount; i++) {
        var string = "";
        for (var j = 0; j < colsAmount; j++) {
            string += matrix[i][j];
        }
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function clearClasses(node){
    node.dom.classList.remove("td-visited");
    node.dom.classList.remove("td-closed");
    node.dom.classList.remove("td-path");
}
function printPath(target){
    var currnet_node = target;
    //td-closed
    while(currnet_node.parent != null){
        path_length++;
        currnet_node.dom.style = null;
        clearClasses(currnet_node);
        currnet_node.dom.classList.add("td-path");
        currnet_node = currnet_node.parent;
    }
    clearClasses(currnet_node);
    currnet_node.dom.classList.add("td-start");
    target.dom.classList.add("td-end");
    target.dom.classList.remove("td-path");
    nodes_expanded_el.innerText = expanded_nodes;
    path_length_el.innerText = path_length + 1;
}

 async function dfs_iter(start, target) {
    // Create a stack for DFS
	let stack = [];
	
	// Push the current source node
	stack.push(start);
	
	while(stack.length != 0){
        // Pop a vertex from stack and print it
		s = stack.pop();
		s.dom.classList.add("td-active");
        await sleep(timeout);
        expanded_nodes++;
        if (s.row === target.row && s.col === target.col) {
            // We have found the goal node we we're searching for
            printPath(target);
            return;
        }
		// Stack may contain same vertex twice. So
		// we need to print the popped item only
		// if it is not visited.
		if (s.visited == false){
			s.visited = true;
		}
		
		// Get all adjacent vertices of the
		// popped vertex s. If a adjacent has
		// not been visited, then push it
		// to the stack.
		for(let node = 0; node < s.neighbors.length; node++){
			if (!s.neighbors[node].visited){
                stack.push(s.neighbors[node])
                s.neighbors[node].parent = s;
            }
				
		}
        s.dom.classList.remove("td-active");
        s.dom.classList.add("td-visited");
    }
 }


 async function bfs_iter(start, target) {
    // Create a stack for DFS
	let queue = [];
	
	// Push the current source node
	queue.push(start);
	
	while(queue.length != 0){
        // Pop a vertex from stack and print it
		s = queue.shift();
		s.dom.classList.add("td-active");
        await sleep(timeout);

        expanded_nodes++;

        if (s.row === target.row && s.col === target.col) {
            // We have found the goal node we we're searching for
            printPath(target);
            return;
        }
		// Stack may contain same vertex twice. So
		// we need to print the popped item only
		// if it is not visited.
		if (s.visited == false){
			s.visited = true;
		}
		
		// Get all adjacent vertices of the
		// popped vertex s. If a adjacent has
		// not been visited, then push it
		// to the queue.
		for(let node = 0; node < s.neighbors.length; node++){
			if (!s.neighbors[node].visited){
                
                s.neighbors[node].wave = s.wave + 1;
                s.neighbors[node].visited = true;
                s.neighbors[node].dom.style.backgroundColor = getColor(s.neighbors[node].wave);
                queue.push(s.neighbors[node]);
                s.neighbors[node].parent = s;
            }
				
		}
        s.dom.classList.remove("td-active");
       // s.dom.classList.add("td-visited");
    }
 }

 async function random_search(start, target){
    let current_node = start;
    while(true){
        if(equalNodes(current_node, target)){
            break;
        }
        if(!current_node.visited){
            expanded_nodes++;
        }
        current_node.visited = true;
        current_node.dom.classList.add("td-active");
        await sleep(timeout);
        //select random node from neighbors
        let neighbors = current_node.neighbors;
        let index = getRandomNumber(neighbors.length);
        current_node.dom.classList.remove("td-active");
        current_node.dom.classList.add("td-visited");

        //next node
        next_node = neighbors[index];
        //set next node parrent attr
        next_node.parent = current_node;

        current_node = next_node;
    } 

    //printPath(target);
    return;
 }

async function greedy_search(start, target){
         // Create a stack for DFS
	let queue = [];
	
	// Push the current source node
	queue.push(start);
	
	while(queue.length != 0){
        // Pop a vertex from stack and print it
        queue.sort((a, b) => a.distance - b.distance)

		s = queue.shift();
		s.dom.classList.add("td-active");
        await sleep(timeout);

        expanded_nodes++;

        if (s.row === target.row && s.col === target.col) {
            // We have found the goal node we we're searching for
            printPath(target);
            return;
        }
		// Stack may contain same vertex twice. So
		// we need to print the popped item only
		// if it is not visited.
		if (s.visited == false){
			s.visited = true;
		}
		
		for(let node = 0; node < s.neighbors.length; node++){
			if (!s.neighbors[node].visited && !s.neighbors[node].closed){
                calculateDistance(s.neighbors[node], target);
                //s.neighbors[node].wave = s.wave + 1;
                s.neighbors[node].visited = true;
                s.neighbors[node].dom.classList.add("td-visited");
                //s.neighbors[node].dom.style.backgroundColor = getColor(s.neighbors[node].wave);
                queue.push(s.neighbors[node]);
                s.neighbors[node].parent = s;
            }
				
		}

        s.closed = true;
        s.dom.classList.remove("td-active");
        s.dom.classList.add("td-closed");
    }
}

async function a_star(start, target){
    // Create a priority queue
	let queue = [];
		
    start.star_distance = 0;
	// Push the current source node
	queue.push(start);

	while(queue.length != 0){
        //something like priority queue :)
        queue.sort((a, b) => function(a, b){
            //heuristics + real cost
            var dist = a.star_distance - b.star_distance;
            return dist;
        });

		s = queue.shift();
		s.dom.classList.add("td-active");
        await sleep(timeout);

        expanded_nodes++;

        if (equalNodes(s, target)) {
            // We have found the goal node we we're searching for
            printPath(target);
            return;
        }
		// Stack may contain same vertex twice. So
		// we need to print the popped item only
		// if it is not visited.
		/*if (s.visited == false){
			s.visited = true;
		}*/
		
		for(let node = 0; node < s.neighbors.length; node++){
            var next_node = s.neighbors[node];
			if (!next_node.closed){
                var new_star_distance = calculateDistanceStar(next_node, target);
                if(!next_node.visited || next_node.star_distance  > new_star_distance){

                    next_node.star_distance = new_star_distance;
                    next_node.wave = s.wave + 1;
                    next_node.parent = s;

                    if(!next_node.visited){
                        next_node.dom.classList.add("td-visited");
                        next_node.visited = true;
                        queue.push(next_node);
                    }
                }
            }
				
		}

        s.closed = true;
        s.dom.classList.remove("td-active");
        s.dom.classList.add("td-closed");
    }
}
function calculateDistanceStar(start, target){
    var heur_dist = Math.abs(start.row - target.row) + Math.abs(start.col - target.col);
    var steps_taken = start.wawe + 1;
    return heur_dist + steps_taken;
}
function calculateDistance(start, target){
    start.distance = Math.abs(start.row - target.row) + Math.abs(start.col - target.col);
}

function equalNodes(s, target){
    let res = s.row === target.row && s.col === target.col;
    return res;
}
function getColor(wave){
    var R = 172;
    var G = 172;
    var B = 6;

    if(wave > 20){
        wave = 1;
    }

    if(wave % 3 == 0){
        R = (wave * 5) % 255;
    }else if(wave % 3 == 1){
        G = (wave * 5) % 255;
    }else{
        B = (wave * 5) % 255;
    }
    //R *= wave;
   // G *= wave * 5;
    //B = (wave * 5) % 255;
    return `rgb(${R}, ${G}, ${B})`;
}

function getRandomNumber(max){
    return Math.floor(Math.random() * max);
}

